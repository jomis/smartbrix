require 'sinatra'
require 'json'
require '../parser/parser_model.rb'
require '../parser/cpe.rb'

class CveHubService < Sinatra::Application

  configure do
    DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/../entries.db")
  end

  get '/cves' do
    content_type :json
    cpe_string = ""
    if params[:name] 
      if params[:version]
        #cpe_string = CPE.new({:part => "/a", :product => "#{params[:name]}", :version => "#{params[:version]}"})
        # cpe_string = "%:#{params[:name]}%:#{params[:version]}%"
        cpe_string = "%:#{params[:name]}:#{params[:version]}%"
     
        #FIDIUS::NVDParserModel::NVDEntry.all(:vulnerable_software.like => "%#{cpe_string}%").to_json
      else
        cpe_string = "%:#{params[:name]}:%"  
      end
      puts cpe_string
      FIDIUS::NVDParserModel::NVDEntry.all(:vulnerable_software.like => "#{cpe_string}").to_json
    else
      FIDIUS::NVDParserModel::NVDEntry.all.to_json
    end
    
  end
  
  get '/cves/:id' do 
    content_type :json
    puts params[:id]
    FIDIUS::NVDParserModel::NVDEntry.get(params[:id])
  end
  

end