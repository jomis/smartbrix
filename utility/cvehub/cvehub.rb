require './parser/parser.rb'
require 'data_mapper'
require 'dm_noisy_failures'

DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/entries.db")
DataMapper.auto_upgrade!

FIDIUS::NVDParserModel::NVDEntry.auto_upgrade!

puts "Starting parser"
# file = "./data/nvdcve-2.0-recent.xml"
file = "./data/nvdcve-2.0-2014.xml"

entries = FIDIUS::NVDParser.parse_cve_file(file)

entries.map!(&:save)
# puts entries.first.save



