# Author::    FIDIUS (mailto:grp-fidius@tzi.de) 
# License::   Distributes under the same terms as fidius-cvedb Gem

# This module provides the object model for one CVE entry as listed in
# the National Vulnerability Database (nvd.nist.gov). 

require 'data_mapper'
require 'time'

module FIDIUS
  module NVDParserModel

    # Represents an entry from nvd.nist.gov
    class NVDEntry
      include DataMapper::Resource

      property :id, Serial
      property :cve, String
      property :vulnerable_configurations, Text
      property :cvss, Object
      property :vulnerable_software, Text
      property :published_datetime, DateTime
      property :last_modified_datetime, DateTime
      property :cwe, String
      property :summary, Text
      property :references, String

      # attr_accessor :cve, :vulnerable_configurations, :cvss, :vulnerable_software,
      #     :published_datetime, :last_modified_datetime, :cwe, :summary,
      #     :references
      
      def initialize(params)
        self.vulnerable_configurations = params[:vulnerable_configurations].join(",")
        self.vulnerable_software       = params[:vulnerable_software].join(",")
        self.published_datetime        = DateTime.parse(params[:published_datetime])
        self.last_modified_datetime    = DateTime.parse(params[:last_modified_datetime])
        self.cvss       = params[:cvss]
        self.cve        = params[:cve]
        self.cwe        = params[:cwe]
        self.summary    = params[:summary]
        # @references = params[:references]
      end
      
    end
    
    # Contains all fields to represent a reference
    class Reference
      include DataMapper::Resource
      
      property :id, Serial
      property :source, String
      property :link, String
      property :name, String
      
      attr_accessor :source, :link, :name
      
      def initialize(params)
        self.source = params[:source]
        self.link   = params[:link]
        self.name   = params[:name]
      end
      
      def to_s
        "source=#{source}, link=#{link}, name=#{name}"
      end
      
    end
    
    # Contains all fields of an CVSS-Score
    # Underscore is needed because there is an rails model
    # with the same name
    class Cvss_
      include DataMapper::Resource
      
      property :id, Serial
      property :score, String
      property :access_vector, String
      property :access_complexity, String
      property :authentication, String
      property :confidentiality_impact, String
      property :integrity_impact, String
      property :availability_impact, String
      property :source, String
      property :generated_on_datetime, DateTime
      
      
      
      # attr_accessor :score, :access_vector, :access_complexity, :authentication,
 #          :confidentiality_impact, :integrity_impact, :availability_impact,
 #          :source, :generated_on_datetime
      
      def initialize(params)
        self.source         = params[:source]
        self.score          = params[:score]
        self.access_vector  = params[:access_vector]
        self.authentication = params[:authentication]
        self.access_complexity      = params[:access_complexity]
        self.integrity_impact       = params[:integrity_impact]
        self.availability_impact    = params[:availability_impact]
        self.confidentiality_impact = params[:confidentiality_impact]
        self.generated_on_datetime  = DateTime.parse(params[:generated_on_datetime])
      end
      
    end
    
  end
end