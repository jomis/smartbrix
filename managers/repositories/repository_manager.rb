require 'sinatra'

# Dir["./analysers/*.rb"].each {|file| require file }

require '../lib/datastore.rb'

require 'json'

class RepositoryManager < Sinatra::Application
  
  def initialize 
    super()
    puts "Initializing stores"
    # @technical_unit_store = Datastore.new("tu")
   #  @deployment_unit_store = Datastore.new("du")
   #  @infrastructure_specification_store = Datastore.new("is")
   #  @deployment_instance_store = Datastore.new("di")
  end

  get '/whoami' do
    "Repository Manager"
  end
  
  
  post '/is' do
      data = JSON.parse request.body.read
      puts "Received #{data}"
      @infrastructure_specification_store.store(data['name'],data)
  end
  
  
  get '/is' do
    return_message = {}
    if params.has_key?('name')
      
      puts "Retrieving #{params['name']}"
      found = @infrastructure_specification_store.retrieve(params['name'])
      puts "Found #{found}"
      return_message = found
    end
    
    if params.has_key?('search')
      return_message = @infrastructure_specification_store.search(params['search'])
    end
    
    if params.empty?
      return_message=@infrastructure_specification_store.get_all_keys()
    end
    
    return_message.to_json
    
  end
  
  post '/du' do
      data = JSON.parse request.body.read
      puts "Received #{data}"
      @deployment_unit_store.store(data['name'],data)
  end
  
end