# Smart Brix Framework

Proof of Concept Implementation of Smart Brix, a Continuous Evolution Framework for Container Application Deployments.

This repository includes a sample setup to reproduce the evaluations, the sensitive informations have been left empty for security reasons. Please substitute them accordingly with the necessary credentials.

Please also note that this prototype is being actively developed.

![Architecture Overview.png](https://bitbucket.org/repo/rE9ypa/images/2925594679-Architecture%20Overview.png)