Purpose of this rubyscript is to scrape docker hub for docker files of public repos

#To start a local registry which stores the images in the images folder 
/Users/jomis/src/university/prototypes/dockersquid/images


#To start the script (be sure to check the dockersquid.rb first there are several config options)

./startsquid.sh

# Docker Search URLS
https://hub.docker.com/search/?q=*&page=4&isAutomated=0&isOfficial=0&pullCount=0&starCount=1