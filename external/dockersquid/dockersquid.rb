require 'mechanize'
require 'typhoeus'

require 'csv'

agent = Mechanize.new
agent.pluggable_parser.default = Mechanize::Download

search_url = 'https://hub.docker.com/search/?q=*&isAutomated=0&isOfficial=0&pullCount=0&starCount=1'
global_explore = 'https://hub.docker.com/explore/'

page = agent.get(search_url)

break_page_number = 500



relevant_dockerlinks = []
dockerfile_map = {}


# Scrape until no more next links are present, since the pagination is done
# via JS we fake the click.
page_number = 1
loop do
  page_number = page_number + 1
  next_link_present = page.link_with(:text=> page_number.to_s)
  page.links.each do |link|

    if /PULLSDETAILS/.match(link.text) != nil
      relevant_dockerlinks<< link
      print "."
    end
  end
  break unless next_link_present
 
  if page_number == break_page_number
    puts 'Hit page limit of:'+break_page_number.to_s
    break
  end
  
  page = agent.get('https://hub.docker.com/search/?q=*&page='+page_number.to_s+'&isAutomated=0&isOfficial=0&pullCount=0&starCount=1')
end

# Add them to a cvs with the corresponding pull command
#RepositoryDetailsWrapper__pullCommand____wsk2

CSV.open("./dockerurls_set3.csv","w") do |csv|
  
  relevant_dockerlinks.each do |dockerlink|
    begin 
      page = dockerlink.click
      result = page.search(".RepositoryDetailsWrapper__pullCommand____wsk2")
      pullcommand =  result.xpath('//input/@value')
    
      puts "#{dockerlink} with #{pullcommand}"
      csv << [dockerlink,pullcommand]
    rescue => e
      puts "ignoring exception"
      
    end
  end
end



# puts "\nFound:"+relevant_dockerlinks.count.to_s+" potential candidates"
#
# #Get the relevant dockerfile urls from each candidate
#
# relevant_dockerlinks.each do |dockerlink|
#   page = dockerlink.click
#   page.links.each do |link|
#     #puts link.text
#     if /latest/.match(link.text) != nil
#       print "."
#       dockerfile_map[dockerlink.text] = link
#     end
#   end
# end
#
# puts "\nFound:"+dockerfile_map.keys.count.to_s+" corresponding latest (active) dockerfiles"
#
# # Download the files
# dockerfile_map.each do |key,value|
#   begin
#     page = value.click
#     page.links.each do |link|
#
#       #This is hackish and can be done way cleaner
#       if /Raw/.match(link.text) != nil
#         filelink= link.href.sub /\/raw\//, '/'
#         reponame = key[/(^\D*)/,1]
#         version = key[/(\d+.\d+)/,1]
#         filename = reponame+"_"+version
#         puts "Downloading:"+key+" into:"+filename
#         agent.get("https://raw.githubusercontent.com"+filelink).save('./dockerfiles/'+filename)
#       end
#     end
#   rescue SystemCallError
#     puts "Failed for:"+key
#   end
# end



# Efficiently download the relevant docker files over multiple connections

# concurrency = 20
# hydra = Typhoeus::Hydra.new(max_concurrency: concurrency)
#
# dockerfile_map.each do |key,value|
#   puts 'Downloading Filename:'+key+" with link:"+value.text
#   request = Typhoeus::Request.new value
#   #   request.on_complete do |response|
#   #     write_file url, response.body
#   #   end
#   #   hydra.queue request
# end


#
# urls.each do |url|
#   request = Typhoeus::Request.new url
#   request.on_complete do |response|
#     write_file url, response.body
#   end
#   hydra.queue request
# end
#
# hydra.run

